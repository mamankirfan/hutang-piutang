<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Dashboard</title>

    @include('partials.style.style')

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        @include('partials.sidebar')



        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                @include('partials.topbar')

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4" style="margin-top: 90px; margin-left:1500px;">
                        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                            <i class="fas fa-download fa-sm text-white-50" ></i> Tambah Hutang</a>
                    </div>


                    <!-- Content Row -->

                    <div class="container-a " style="display:flex; justify-content:center;">
                        <table class="table" style="margin-top: 10px; width: 90%; ">
                            <thead>
                                <tr style="font-family: bold">
                                    <th>No</th>
                                    <th>Nama peminjam</th>
                                    <th>Dengan kebutuhan</th>
                                    <th>Jumlah Pinjaman</th>
                                    <th>Status</th>
                                    <th>Tanggal Tempo</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1.</td>
                                    <td>Jono</td>
                                    <td>Beli cireng</td>
                                    <td>980.000</td>
                                    <td>belum lunas</td>
                                    <td> Agu . 20 . 12:00 PM</td>
                                    <td>hmm</td>
                                </tr>
                                <tr>
                                    <td>2.</td>
                                    <td>Suminem</td>
                                    <td>beli telur gulung</td>
                                    <td>1.000.000</td>
                                    <td>belum lunas</td>
                                    <td> Agu . 20 . 12:00 PM</td>
                                    <td>hmm</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->


            </div>
            @include('partials.footer')
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>

        @include('partials.script.script')

</body>

</html>
