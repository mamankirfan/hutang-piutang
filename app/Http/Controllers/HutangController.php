<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HutangController extends Controller
{
    public function dashboard()
    {
        return view("dashboard");
    }

    public function tambah_hutang()
    {
        return view("hutang");
    }

    public function tambah_piutang()
    {
        return view("piutang");
    }

    public function profile()
    {
        return view("profile");
    }

    public function login()
    {
        return view("login");
    }
}
